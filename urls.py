# Copyright 2014 University of Alaska Anchorage Experimental Economics
# Laboratory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('banking.views',

        # Views for the "banking" experiment type
        url(r'^$', 'index', name='index'),
        url(r'^participate/?$', 'participant_index', name='participant_index'),
        url(r'^experiment/?$', 'experimenter_index', name='experimenter_index'),

        # Views for a specific experiment
        url(r'^(?P<experiment_id>\d+)/experimenter$', 'experimenter',
            name='experimenter'),
        url(r'^(?P<experiment_id>\d+)/set_visibility$', 'set_visibility',
            name='set_visibility'),
        url(r'^(?P<experiment_id>\d+)/configure$', 'configure',
            name='configure'),
        url(r'^(?P<experiment_id>\d+)/allocate-groups$', 'allocate_groups',
            name='allocate_groups'),
        url(r'^(?P<experiment_id>\d+)/remove-participant$', 'remove_participant',
            name='remove_participant'),
        url(r'^(?P<experiment_id>\d+)/advance$', 'advance',
            name='advance'),
        url(r'^(?P<experiment_id>\d+)/reset$', 'reset',
            name='reset'),
        url(r'^(?P<experiment_id>\d+)/download-data$', 'download_data',
            name='download_data'),
        url(r'^(?P<experiment_id>\d+)/email-data$', 'email_data',
            name='email_data'),
        url(r'^(?P<experiment_id>\d+)/participate$', 'participate',
            name='participate'),
        url(r'^(?P<experiment_id>\d+)/submit-decision$', 'submit_decision',
            name='submit_decision'),
        url(r'^(?P<experiment_id>\d+)/timer_expiration$', 'timer_expiration',
            name='timer_expiration'),

        # Clock synchronization
        url(r'^gettime$', 'gettime', name='gettime'),

        # Test URLs
        url(r'^participate-test/?$', 'participate_test'),
        url(r'^server-clock/?$', 'server_clock'),
        )
