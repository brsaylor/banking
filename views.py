# Copyright 2014 University of Alaska Anchorage Experimental Economics
# Laboratory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import time
from decimal import Decimal as D
import copy
import random
import csv
import os
import tempfile
import zipfile

from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, redirect, get_object_or_404,\
        render
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.decorators.http import require_GET, require_POST
from django.core.urlresolvers import reverse
from django.forms.models import modelformset_factory
from django.core.mail import EmailMessage
from django.contrib import messages

from vcweb.core.decorators import participant_required, experimenter_required
from vcweb.core.models import (is_participant, is_experimenter, Experiment,
        ParticipantExperimentRelationship, ParticipantGroupRelationship,
        Parameter, RoundConfiguration, Participant)

from banking.models import *
from banking.forms import *

logger = logging.getLogger('vcweb') # FIXME - __name__ doesn't show up in logs

@login_required
def index(request):
    if is_participant(request.user):
        logger.info('user is a participant')
        return redirect('banking:participant_index')
    elif is_experimenter(request.user):
        return redirect('banking:experimenter_index')
    else:
        logger.warning("user %s isn't an experimenter or participant",
                request.user)
        return redirect('home')

@login_required
@user_passes_test(is_experimenter)
def experimenter_index(request):
    experimenter = request.user.experimenter
    experiments = experimenter.experiment_set.filter(
            experiment_metadata=get_experiment_metadata())
    return render_to_response('banking/experimenter-index.html', locals(),
            context_instance=RequestContext(request))

@login_required
@user_passes_test(is_participant)
def participant_index(request):
    participant = request.user.participant
    experiment_dict = {}
    for experiment in participant.experiments.filter(
            experiment_metadata=get_experiment_metadata()):
        status = experiment.get_status_display()
        if not status in experiment_dict:
            experiment_dict[status] = list()
        experiment_dict[status].append(experiment)
    return render_to_response('banking/participant-index.html', locals(),
            context_instance=RequestContext(request))

@login_required
@user_passes_test(is_experimenter)
def configure(request, experiment_id=None):
    """
    Experiment configuration form
    """
    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())

    # Try to get the ExperimentState for the experiment.
    # If it does not exist, that means the experiment has not yet started.
    try:
        state = ExperimentState.objects.get(experiment=experiment)
    except ExperimentState.DoesNotExist:
        state = None

    ParameterFormset = modelformset_factory(
            RoundParameterSet, extra=0,
            exclude=('round_number', 'experiment',), can_delete=True)

    if request.method == 'POST':

        formset = ParameterFormset(
                request.POST, request.FILES,
                queryset=RoundParameterSet.objects.filter(
                    experiment=experiment))

        if formset.is_valid():

            # Set values of excluded fields (experiment, round_number). If no
            # fields were excluded, the block below could be replaced by
            # formset.save().
            round_number = 1
            for form in formset:
                params = form.save(commit=False)
                if form.cleaned_data.get('DELETE'):
                    params.delete()
                else:
                    params.experiment = experiment
                    params.round_number = round_number
                    params.save()
                    round_number += 1

            if request.POST.get('add_round'):
                RoundParameterSet.objects.create(experiment=experiment,
                        round_number=round_number)
                return redirect('banking:configure',
                        experiment_id=experiment_id)

            else:
                return redirect('banking:experimenter',
                        experiment_id=experiment_id)

    else:
        formset = ParameterFormset(queryset=RoundParameterSet.objects.filter(
            experiment=experiment))

    return render(request, 'banking/configure.html', {
        'experiment': experiment,
        'formset': formset,
        'state': state,
        })

def get_num_rounds(experiment):
    """ Return the number of rounds in the given experiment."""
    return RoundParameterSet.objects.filter(experiment=experiment).count()

@login_required
@user_passes_test(is_experimenter)
def experimenter(request, experiment_id=None):
    """
    The main experimenter view
    """
    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())

    # Try to get the ExperimentState for the experiment.
    # If it does not exist, that means the experiment has not yet started.
    try:
        state = ExperimentState.objects.get(experiment=experiment)
    except ExperimentState.DoesNotExist:
        state = None

    # Build the participant status table
    experiment_participants = list(experiment.participant_set.all())
    group_relationships = list(
            ParticipantGroupRelationship.objects.by_experiment(experiment)
            .order_by('group__number', 'participant_number'))
    grouped_participants = [pgr.participant for pgr in group_relationships]
    ungrouped_participants = (
            set(experiment_participants) - set(grouped_participants))
    participant_status_table = []
    for pgr in group_relationships:
        row = {
                'participant': pgr.participant,
                'group_number': pgr.group.number,
                'bank_number': pgr.participant_number,
                }
        # Get PeriodData records for this participant in this round
        if state is not None:
            period_data = list(PeriodData.objects.filter(
                    participant_group_relationship=pgr,
                    round_number=state.round_number).order_by('period_number'))
            if len(period_data) > 0:
                # There is at least data for Period 1
                row['region_number'] = period_data[0].region_number
                row['depositDecision'] = period_data[0].depositDecision
                row['lendingDecision'] = period_data[0].lendingDecision
                row['earnings'] = period_data[-1].earnings
        participant_status_table.append(row)
    for participant in ungrouped_participants:
        participant_status_table.append({'participant': participant})

    # Build parameters table
    round_parameter_sets = RoundParameterSet.objects.filter(
            experiment=experiment).order_by('round_number')

    logger.debug("round_parameter_sets.count() = " + str(round_parameter_sets.count()))

    # If there are no RoundParameterSets for this experiment, it may be a cloned
    # experiment that is being viewed for the first time. In that case, copy the
    # RoundParameterSets from the experiment it was cloned from.
    if round_parameter_sets.count() == 0:
        logger.info("No RoundParameterSets for experiment" + str(experiment))
        try:
            orig_exp = (experiment.experiment_configuration.experiment_set
                    .exclude(id=experiment.id)
                    .order_by('-id'))[0]
        except IndexError:
            logger.info("No other experiment with this ExperimentConfiguration")
        else:
            logger.info("Cloning RoundParameterSets from experiment " +
                    str(orig_exp))
            for rps in orig_exp.roundparameterset_set.all():
                rps.id = None
                rps.experiment = experiment
                rps.save()

    return render(request, 'banking/experimenter.html', {
        'experiment': experiment,
        'state': state,
        'num_rounds': get_num_rounds(experiment),
        'participant_status_table': participant_status_table,
        'round_parameter_set_opts': RoundParameterSet._meta,
        'round_parameter_sets': round_parameter_sets,
        })

@login_required
@user_passes_test(is_experimenter)
@require_POST
def set_visibility(request, experiment_id=None):
    """ Set the is_visible and afield and of the experiment and the
    authentication_code fields of the experiment. """

    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())

    experiment.is_visible = (request.POST.get('is_visible') == '1')
    experiment.authentication_code = request.POST.get(
            'authentication_code', '').strip()
    experiment.save()

    return redirect('banking:experimenter', experiment_id=experiment_id)

def get_data_csv(experiment):
    """ Write the PeriodData for the given experiment to a temporary CSV file
    and return the file. The returned object is a tempfile.NamedTemporaryFile
    which will be deleted as soon as it is closed. """

    field_names = [
            'experiment_id',
            'participant_id',
            'email',
            'group_number',
            'bank_number',
            'region_number',
            'depositee_number',
            'depositor_number',
            ]
    field_names.extend([field.name for field in PeriodData._meta.fields])
    field_names.remove('id')
    field_names.append('equity')

    f = tempfile.NamedTemporaryFile() 
    writer = csv.DictWriter(f, field_names)

    # writeheader() not available in Python 2.6
    #writer.writeheader() 
    header_row = {}
    for fname in field_names:
        header_row[fname] = fname
    writer.writerow(header_row)

    period_data = PeriodData.objects.filter(
            participant_group_relationship__group__experiment=experiment
            ).order_by(
                    'round_number',
                    'period_number',
                    'participant_group_relationship__group__number',
                    'participant_group_relationship__participant_number',
                    )
    for pd in period_data:
        row = {
                'experiment_id': (pd.participant_group_relationship
                    .group.experiment.id),
                'participant_id': (pd.participant_group_relationship
                    .participant.id),
                'email': pd.participant_group_relationship.participant.email,
                'group_number': pd.group_number,
                'round_number': pd.round_number,
                'period_number': pd.period_number,
                'bank_number': pd.bank_number,
                'region_number': pd.region_number,
                'depositee_number': pd.depositee_number,
                'depositor_number': pd.depositor_number,
                'cash': pd.cash,
                'onDepositAtOtherBank': pd.onDepositAtOtherBank,
                'loans': pd.loans,
                'deposits': pd.deposits,
                'depositFromOtherBank': pd.depositFromOtherBank,
                'borrowings': pd.borrowings,
                'depositDecision': pd.depositDecision,
                'lendingDecision': pd.lendingDecision,
                'withdrawals': pd.withdrawals,
                'recalledByOtherBank': pd.recalledByOtherBank,
                'totalOwed': pd.totalOwed,
                'paidFromCash': pd.paidFromCash,
                'paidFromRecalledFunds': pd.paidFromRecalledFunds,
                'loansSold': pd.loansSold,
                'paidFromLoans': pd.paidFromLoans,
                'paidFromBorrowing': pd.paidFromBorrowing,
                'withdrawalByOtherBank': pd.withdrawalByOtherBank,
                'bankPayoff': pd.bankPayoff,
                'loanPayoff': pd.loanPayoff,
                'earnings': pd.earnings,
                'economy': pd.economy,
                'equity': pd.equity,
                }
        writer.writerow(row)

    f.seek(0)
    return f

def get_parameters_csv(experiment):
    """ Write the RoundParameterSets for the given experiment to a temporary CSV
    file and return the file. The returned object is a
    tempfile.NamedTemporaryFile which will be deleted as soon as it is closed.
    """
    
    field_names = [f.name for f in RoundParameterSet._meta.fields]
    field_names.remove('id')

    f = tempfile.NamedTemporaryFile()
    writer = csv.DictWriter(f, field_names)

    header_row = {}
    for fname in field_names:
        header_row[fname] = fname
    writer.writerow(header_row)
    writer.writerows(RoundParameterSet.objects
            .filter(experiment=experiment)
            .values(*field_names))

    f.seek(0)
    return f

@login_required
@user_passes_test(is_experimenter)
def download_data(request, experiment_id=None):
    """ Return a ZIP file containing the RoundParameterSets and PeriodData for
    this experiment. """

    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())

    # Generate the CSV files
    parameters_csv = get_parameters_csv(experiment)
    data_csv = get_data_csv(experiment)

    # Create a temporary ZIP file and write the CSV files to it
    f = tempfile.NamedTemporaryFile()
    z = zipfile.ZipFile(f, 'w', zipfile.ZIP_DEFLATED)
    z.write(parameters_csv.name,
            'banking_{0}_parameters.csv'.format(experiment.id))
    z.write(data_csv.name,
            'banking_{0}_period_data.csv'.format(experiment.id))
    z.close()
    f.seek(0)

    response = HttpResponse(f, mimetype='application/zip')
    response['Content-Disposition'] = ('attachment; filename=' +
            'banking_{0}_data.zip'.format(experiment.id))

    return response

@login_required
@user_passes_test(is_experimenter)
@require_POST
def email_data(request, experiment_id=None):
    """ Email CSVs of the RoundParameterSets and PeriodData to the requested
    address. """

    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())

    # Generate the CSV files
    parameters_csv = get_parameters_csv(experiment)
    data_csv = get_data_csv(experiment)

    recipient = request.POST.get('recipient')
    subject = "Experiment Data for Banking #{0}".format(experiment.id)
    body = "Parameters and output data for Banking #{0} are attached.".format(
            experiment.id)
    message = EmailMessage(
            subject, body, request.user.email, [recipient])
    message.attach('banking_{0}_parameters.csv'.format(experiment.id),
            parameters_csv.read(), 'text/csv')
    message.attach('banking_{0}_period_data.csv'.format(experiment.id),
            data_csv.read(), 'text/csv')
    message.send()

    messages.info(request, "Experiment data was emailed to " + recipient)

    return redirect('banking:experimenter', experiment_id=experiment_id)

@login_required
@user_passes_test(is_experimenter)
@require_POST
def allocate_groups(request, experiment_id=None):
    """
    Assign participants to groups
    """
    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())
    
    experiment.allocate_groups()

    return redirect('banking:experimenter', experiment_id=experiment_id)

@login_required
@user_passes_test(is_experimenter)
@require_POST
def remove_participant(request, experiment_id=None):
    """
    Remove a participant from the experiment
    """

    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())

    redir = redirect('banking:experimenter', experiment_id=experiment_id)

    # Try to get the Participant
    participant_id = request.POST.get('participant_id')
    try:
        participant = Participant.objects.get(pk=participant_id)
    except Participant.DoesNotExist:
        # Unlikely to happen
        messages.error(request, "No participant with id {0}"
                .format(participant_id))
        return redir

    # Try to get a ParticipantExperimentRelationship
    try:
        per = ParticipantExperimentRelationship.objects.get(
                experiment=experiment, participant=participant)
    except ParticipantExperimentRelationship.DoesNotExist:
        # Unlikely to happen
        messages.error(request, "Participant {0} is not in experiment #{1}"
                .format(participant, experiment.id))
        return redir

    # Try to get a ParticipantGroupRelationship
    try:
        pgr = ParticipantGroupRelationship.objects.get(
                group__experiment=experiment, participant=participant)
    except ParticipantGroupRelationship.DoesNotExist:
        pgr = None

    if pgr is not None and experiment.status != Experiment.Status.INACTIVE:
        messages.error(request,
                "Not removing participant {0}, because the experiment is in "
                " progress and the participant is assigned to a group."
                .format(participant))
        return redir

    if pgr is not None:
        pgr.delete()
    per.delete()
    messages.info(request, "Participant {0} removed from experiment"
            .format(participant))

    return redir

def create_period_data(state, prev_period_data=None):
    """
    Create period data for current period, carrying over previous values if
    prev_period_data is supplied.
    """
    period_data = []

    if prev_period_data is None:
        # There is no previous period data.
        # Create the new period data from scratch.
        for pgr in state.experiment.participant_group_relationships:
            period_data.append(PeriodData(
                participant_group_relationship=pgr,
                round_number=state.round_number,
                period_number=state.period_number,
                cash=Decimal('100.00'),
                onDepositAtOtherBank=Decimal('0.00'),
                loans=Decimal('0.00'),
                deposits=Decimal('100.00'),
                depositFromOtherBank=Decimal('0.00'),
                borrowings=Decimal('0.00'),
                earnings=Decimal('0.00'),
                ))

    elif state.period_number == 1:
        # There is previous period data, but this is the first period of a new
        # round, so copy only earnings from previous period.
        for prev_pd in prev_period_data:
            pgr = prev_pd.participant_group_relationship
            period_data.append(PeriodData(
                participant_group_relationship=pgr,
                round_number=state.round_number,
                period_number=state.period_number,
                cash=Decimal('100.00'),
                onDepositAtOtherBank=Decimal('0.00'),
                loans=Decimal('0.00'),
                deposits=Decimal('100.00'),
                depositFromOtherBank=Decimal('0.00'),
                borrowings=Decimal('0.00'),
                earnings=prev_pd.earnings,
                ))

    else:
        # Copy previous period data.
        for prev_pd in prev_period_data:
            pd = copy.copy(prev_pd)
            pd.pk = None
            pd.round_number = state.round_number
            pd.period_number = state.period_number
            period_data.append(pd)
    
    for pd in period_data:
        pd.save()

    return period_data

def group_period_data(period_data):
    """
    Given a QuerySet or list of PeriodData for a single round and period, return
    a dictionary D such that D[group_number][bank_number] is the PeriodData for
    the Participant with group_number and bank_number.
    """
    grouped_period_data = {}
    for pd in period_data:
        if not grouped_period_data.has_key(pd.group_number):
            grouped_period_data[pd.group_number] = {}
        grouped_period_data[pd.group_number][pd.bank_number] = pd
    return grouped_period_data


@login_required
@user_passes_test(is_experimenter)
@require_POST
def reset(request, experiment_id=None):
    """
    Reset the experiment by deleting the ExperimentState and all PeriodData
    objects for this experiment.
    """
    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())
    experiment.status = Experiment.Status.INACTIVE
    experiment.save()

    try:
        state = ExperimentState.objects.get(experiment=experiment)
        state.delete()
    except ExperimentState.DoesNotExist:
        pass

    PeriodData.objects.filter(
            participant_group_relationship__group__experiment=experiment
            ).delete()

    request.session['auto_advance'] = False

    return redirect('banking:experimenter', experiment_id=experiment_id)

@login_required
@user_passes_test(is_experimenter)
@require_POST
def advance(request, experiment_id=None):
    """
    Calculate and store the results of the current (sub)period
    and advance to the next one
    """
    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(
            Experiment.objects.select_related(),
            pk=experiment_id, experiment_metadata=get_experiment_metadata())

    # Try to get the ExperimentState for the experiment.
    # If it does not exist, that means the experiment has not yet started.
    try:
        state = ExperimentState.objects.get(experiment=experiment)
        experiment_starting = False
    except ExperimentState.DoesNotExist:
        # Start the experiment.
        state = ExperimentState(
                experiment=experiment,
                round_number=1,
                period_number=1,
                )
        experiment_starting = True

        # ROUND_IN_PROGRESS would be a more accurate status, but causes the
        # experimenter dashboard to crash because there is no associated
        # RoundData.
        experiment.status = Experiment.Status.ACTIVE
        experiment.save()

    # If the timer is going to expire within 3 seconds, wait for it, because the
    # request probably came in early due to clock sync error.
    if state.ms_remaining <= 3000:
        time.sleep(state.ms_remaining / 1000.0)

    # Force the timer to expire
    state.set_timer_sec(0)
    state.save()

    # Get the parameters for the current round
    try:
        params = RoundParameterSet.objects.get(experiment=experiment,
                round_number=state.round_number)
    except RoundParameterSet.DoesNotExist:
        # Whoops! There are no parameters for this round!
        return redirect('banking:experimenter', experiment_id=experiment_id)

    # Get all PeriodData for the current period
    period_data = PeriodData.objects.filter(
            participant_group_relationship__group__experiment=experiment,
            round_number=state.round_number,
            period_number=state.period_number)

    # Depending on which round and period it is, whether the hedging parameter
    # is True, and whether the experiment has just started or a (sub)period is
    # ending, perform some or all of the following actions, in order:
    # 0. Perform start-of-experiment initialization
    # 1. Calculate and store the results of the deposit or lending decisions
    # 2. Increment the round, period, and/or subperiod
    # 3. Create new PeriodData objects for the new period, carrying over
    #    previous values
    # 4. Calculate the results for the new period (2 or 3)
    # 5. Start the timer
    if experiment_starting:
        if params.hedging:
            state.subperiod = 'deposit'
        create_period_data(state)
        if params.hedging:
            state.set_timer_sec(params.period_1_deposit_time_limit)
        else:
            state.set_timer_sec(params.period_1_lending_time_limit)

    elif state.period_number == 1 and not params.hedging:
        calculate_p1_lending_results(period_data, params)
        state.period_number = 2
        new_period_data = create_period_data(state, period_data)
        calculate_p2_results(new_period_data, params)
        state.set_timer_sec(params.period_2_time_limit)

    elif state.period_number == 1 and state.subperiod == 'deposit':
        calculate_p1_deposit_results(period_data, params)
        state.subperiod = 'lending'
        state.set_timer_sec(params.period_1_lending_time_limit)

    elif state.period_number == 1 and state.subperiod == 'lending':
        calculate_p1_lending_results(period_data, params)
        state.period_number = 2
        state.subperiod = None
        new_period_data = create_period_data(state, period_data)
        calculate_p2_results(new_period_data, params)
        state.set_timer_sec(params.period_2_time_limit)

    elif state.period_number == 2:
        state.period_number = 3
        new_period_data = create_period_data(state, period_data)
        calculate_p3_results(new_period_data, params)
        state.set_timer_sec(params.period_3_time_limit)

    elif state.period_number == 3:
        state.round_number += 1
        state.period_number = 1
        try:
            new_params = RoundParameterSet.objects.get(experiment=experiment,
                    round_number=state.round_number)
        except RoundParameterSet.DoesNotExist:
            # Whoops! There are no parameters for this round!
            return redirect('banking:experimenter', experiment_id=experiment_id)
        if new_params.hedging:
            state.subperiod = 'deposit'
        create_period_data(state, period_data)
        if new_params.hedging:
            state.set_timer_sec(new_params.period_1_deposit_time_limit)
        else:
            state.set_timer_sec(new_params.period_1_lending_time_limit)

    else:
        request.session['error'] = (
                "Serious error: The experiment is in an unexpected state: " +
                "experiment_starting={0}, period_number={1}, " +
                "hedging={2}, subperiod={3}").format(
                        experiment_starting,
                        period_number,
                        hedging,
                        subperiod)

    # Save the experiment state!
    state.save()

    request.session['auto_advance'] = bool(request.POST.get('auto_advance'))

    return redirect('banking:experimenter', experiment_id=experiment_id)

def calculate_p1_lending_results(period_data, params):
    """ Calculate lending decision results. """
    for pd in period_data:
        if pd.lendingDecision is None:
            # Default lending decision is 0
            pd.lendingDecision = Decimal('0.00')
            pd.loans = pd.lendingDecision
            pd.cash -= pd.loans
            pd.save()
        # If pd.lendingDecision is not None, then the results were already
        # calculated for this participant when they submitted their lending
        # decision.

def calculate_p1_deposit_results(period_data, params):
    """ Calculate interbank deposit results. """
    for pd in period_data:
        if pd.depositDecision is None:
            # Default deposit decision is 0
            pd.depositDecision = Decimal('0.00')
    grouped_period_data = group_period_data(period_data)
    for group_number, bank_data in grouped_period_data.items():
        for bank_number, pd in bank_data.items():
            depositee_pd = bank_data[pd.depositee_number]
            pd.cash -= pd.depositDecision
            depositee_pd.cash += pd.depositDecision
            pd.onDepositAtOtherBank = pd.depositDecision
            depositee_pd.depositFromOtherBank = pd.depositDecision
    for pd in period_data:
        pd.save()

def calculate_p2_results(period_data, params):
    """ Calculate results of impatient withdrawals. """

    # Randomly determine the economy and impatient withdrawals by region.
    economy_by_region = ['normal', 'recession']
    withdrawals_by_region = [params.impatient_withdrawals,
            100 - params.impatient_withdrawals]
    if random.choice([True, False]):
        economy_by_region.reverse()
        withdrawals_by_region.reverse()
    if not params.complementarity:
        economy_by_region[1] = economy_by_region[0]
        withdrawals_by_region[1] = withdrawals_by_region[0]

    # Calculate the results for each group.
    grouped_period_data = group_period_data(period_data)
    for group_number, bank_data in grouped_period_data.items():

        # Indexed by bank_number, the total amount immediately owed by the bank,
        # reduced each time a payment from any source is made.  Also, if
        # hedging, it is increased when the other bank recalls funds.
        # pd.totalOwed is not reduced when payments are made.
        owed = {}

        # Assign impatient depositor withdrawals.
        for bank_number, pd in bank_data.items():
            pd.economy = economy_by_region[pd.region_number - 1]
            pd.withdrawals = withdrawals_by_region[pd.region_number - 1]
            owed[pd.bank_number] = pd.withdrawals
            pd.totalOwed = pd.withdrawals

        # Make payments to impatient depositors from cash.
        for bank_number, pd in bank_data.items():
            pd.deposits -= pd.withdrawals
            pd.paidFromCash = min(owed[bank_number], pd.cash)
            pd.cash -= pd.paidFromCash
            owed[bank_number] -= pd.paidFromCash

        if params.hedging:
            # Banks now recall funds as necessary to repay what they still
            # owe.  There is sort of a circular dependency: the amount owed
            # by a bank depends on the amount recalled by the previous bank,
            # which depends on what that previous bank owes.  This is solved
            # by repeatedly traversing the cyclical network of banks from
            # depositor to depositee until each bank has been reached at
            # least once (so each bank has the opportunity to recall funds),
            # and the current bank does not recall any more funds (which
            # means the next bank will not need to recall more).
            #
            # Note that this loop will terminate because there is a finite
            # amount that is available to recall.

            # Initialize the recalled variables
            for bank_number, pd in bank_data.items():
                pd.recalledByOtherBank = Decimal('0.00')
                pd.paidFromRecalledFunds = Decimal('0.00')

            traversal = 0  # Number of full traversals of the network
            pd = bank_data[1] # Start with the first bank in the group
            while True:  # Iterates once per bank

                bank_number = pd.bank_number

                # Pay as much as possible from cash (although this has
                # already been done for repaying impatient depositors, more
                # may be owed now to the other bank recalling funds).
                paidCash = min(owed[bank_number], pd.cash)
                pd.cash -= paidCash
                pd.paidFromCash += paidCash
                owed[bank_number] -= paidCash

                # Pay as much as possible by recalling funds.
                recalled = min(owed[bank_number], pd.onDepositAtOtherBank)

                # If we've gone around at least once and this bank is not
                # recalling any more funds, then no banks will be recalling
                # any more funds, so we're done.
                if traversal > 0 and recalled <= 0:
                    break

                # Otherwise, execute the recall.
                pd.onDepositAtOtherBank -= recalled
                depositee_pd = bank_data[pd.depositee_number]
                depositee_pd.recalledByOtherBank += recalled
                depositee_pd.depositFromOtherBank -= recalled
                owed[depositee_pd.bank_number] += recalled
                depositee_pd.totalOwed += recalled
                pd.paidFromRecalledFunds += recalled
                owed[bank_number] -= recalled

                # Move on to the next bank in the network, the depositee.
                pd = depositee_pd
                if pd == bank_data[1]:
                    traversal += 1

        # Calculate additional payments from loan sales and borrowing.
        for bank_number, pd in bank_data.items():

            # Sell and repay from loans
            if owed[bank_number] > 0:
                pd.loansSold = min(
                        owed[bank_number] / params.loan_price, pd.loans
                        ).quantize(Decimal('0.00'))
                pd.loans -= pd.loansSold
                pd.paidFromLoans = (pd.loansSold * params.loan_price
                        ).quantize(Decimal('0.00'))
                owed[bank_number] -= pd.paidFromLoans
            else:
                pd.loansSold = Decimal('0.00')
                pd.paidFromLoans = Decimal('0.00')

            # Borrowing covers the remainder of what's owed.
            if owed[bank_number] > 0:
                pd.paidFromBorrowing = owed[bank_number]
                pd.borrowings = pd.paidFromBorrowing
                owed[bank_number] = Decimal('0.00')
            else:
                pd.paidFromBorrowing = Decimal('0.00')
                pd.borrowings = Decimal('0.00')

    for pd in period_data:
        pd.save()

def calculate_p3_results(period_data, params):
    """ Calculate the final round results. """

    # Calculate the results for each group.
    grouped_period_data = group_period_data(period_data)
    for group_number, bank_data in grouped_period_data.items():

        owed = {}

        for bank_number, pd in bank_data.items():

            # Calculate loan payoff (principal + interest).
            pd.loanPayoff = (pd.loans * (params.loan_interest_rate + 1)
                    ).quantize(Decimal('0.00'))
            pd.loans = Decimal('0.00')
            pd.cash += pd.loanPayoff

            # Calculate withdrawals by patient depositors.
            pd.withdrawals = (pd.deposits * (params.deposit_interest_rate + 1)
                    ).quantize(Decimal('0.00'))
            pd.deposits = Decimal('0.00')
            owed[bank_number] = pd.withdrawals

            if params.hedging:
                # Calculate payoff from bank deposit.
                pd.bankPayoff = (pd.onDepositAtOtherBank
                        * (params.bank_interest_rate + 1)
                        ).quantize(Decimal('0.00'))
                pd.onDepositAtOtherBank = Decimal('0.00')
                pd.cash += pd.bankPayoff

                # Calculate withdrawal by other bank.
                pd.withdrawalByOtherBank = (pd.depositFromOtherBank *
                        (params.bank_interest_rate + 1)
                        ).quantize(Decimal('0.00'))
                pd.depositFromOtherBank = Decimal('0.00')
                owed[bank_number] += pd.withdrawalByOtherBank

            pd.totalOwed = owed[bank_number]

            # Pay from cash (which now includes payoff from loans and
            # interbank deposit)
            pd.paidFromCash = min(owed[bank_number], pd.cash)
            pd.cash -= pd.paidFromCash
            owed[bank_number] -= pd.paidFromCash

            # Pay from borrowing
            if owed[bank_number] > 0:
                pd.paidFromBorrowing = owed[bank_number]
                pd.borrowings += pd.paidFromBorrowing
                owed[bank_number] = Decimal('0.00')
            else:
                pd.paidFromBorrowing = Decimal('0.00')

            # Update the earnings,
            # which is a cumulative sum of equity at the end of each round.
            if not params.practice:
                pd.earnings += pd.equity

    for pd in period_data:
        pd.save()

@login_required
@user_passes_test(is_participant)
def participate(request, experiment_id=None):

    participant = request.user.participant
    
    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(Experiment.objects,
            pk=experiment_id,
            experiment_metadata=get_experiment_metadata())

    # If the participant is not in this experiment, redirect to participant
    # dashboard
    if not (ParticipantExperimentRelationship.objects
            .filter(participant=participant, experiment=experiment)
            .exists()):
        messages.error(request,
                "You are not currently participating experiment #{0}."
                .format(experiment.id))
        return redirect('core:dashboard')

    # Try to get the ExperimentState for the experiment.
    try:
        state = ExperimentState.objects.get(experiment=experiment)
    except ExperimentState.DoesNotExist:
        # The experiment has not yet started.
        state = None
        params = None
        period_data = []
        bank_number = None
        region_number = None
        earnings = Decimal('0.00')
    else:
        # The experiment has started.

        # Get the parameters for the current round of the experiment
        params = get_object_or_404(RoundParameterSet.objects,
                experiment=experiment, round_number=state.round_number)

        # Get the PeriodData for the current round for this experiment and
        # participant
        period_data = list(PeriodData.objects.filter(
                participant_group_relationship__group__experiment=experiment,
                participant_group_relationship__participant=participant,
                round_number=state.round_number))
        
        # Get bank number, region number, and current earnings
        bank_number = period_data[-1].bank_number
        region_number = period_data[-1].region_number
        earnings = period_data[-1].earnings

    return render(request, 'banking/participate.html', {
            'experiment': experiment,
            'state': state,
            'num_rounds': get_num_rounds(experiment),
            'params': params,
            'period_data': period_data,
            'bank_number': bank_number,
            'region_number': region_number,
            'earnings': earnings,
        })

@login_required
def timer_expiration(request, experiment_id=None):
    """
    Return the timer_expiration for the ExperimentState of the given experiment.
    """
    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(Experiment.objects,
            pk=experiment_id,
            experiment_metadata=get_experiment_metadata())

    # Try to get the ExperimentState for the experiment.
    try:
        state = ExperimentState.objects.get(experiment=experiment)
        timer_expiration = state.timer_expiration
    except ExperimentState.DoesNotExist:
        timer_expiration = 0

    return HttpResponse(timer_expiration)

@login_required
@user_passes_test(is_participant)
def submit_decision(request, experiment_id=None):
    """
    Store a submitted deposit or lending decision.
    """

    participant = request.user.participant
    
    # Try to get the banking Experiment with the given id
    experiment = get_object_or_404(Experiment.objects,
            pk=experiment_id,
            experiment_metadata=get_experiment_metadata())

    # Try to get the ExperimentState for the experiment.
    state = get_object_or_404(ExperimentState.objects, experiment=experiment)

    # Get the PeriodData for the current round and period
    # for this experiment and participant
    pd = PeriodData.objects.get(
            participant_group_relationship__group__experiment=experiment,
            participant_group_relationship__participant=participant,
            round_number=state.round_number,
            period_number=state.period_number)

    # If a deposit decision is expected and is provided, validate and store it
    if (state.period_number == 1
            and state.ms_remaining > 0
            and state.subperiod == 'deposit'
            and pd.depositDecision is None
            and 'depositDecision' in request.POST):
        try:
            depositDecision = Decimal(int(request.POST['depositDecision']))
        except:
            depositDecision = Decimal(0)
        if depositDecision < 0:
            depositDecision = Decimal(0)
        elif depositDecision > pd.cash:
            depositDecision = pd.cash
        pd.depositDecision = depositDecision
        pd.save()

    # If a lending decision is expected and is provided, validate and store it
    elif (state.period_number == 1
            and state.ms_remaining > 0
            and (state.subperiod == 'lending' or state.subperiod is None)
            and pd.lendingDecision is None
            and 'lendingDecision' in request.POST):
        try:
            lendingDecision = Decimal(int(request.POST['lendingDecision']))
        except:
            lendingDecision = Decimal(0)
        if lendingDecision < 0:
            lendingDecision = Decimal(0)
        elif lendingDecision > pd.cash:
            lendingDecision = pd.cash
        pd.lendingDecision = lendingDecision

        # The results of the lending decision can be calculated immediately.
        pd.loans = pd.lendingDecision
        pd.cash -= pd.loans

        pd.save()

    # Otherwise, ignore it

    return redirect('banking:participate', experiment_id=experiment.id)

# Test views

def participate_test(request):
    experiment_state = ExperimentState(
            experiment=None,
            round_number=1,
            period_number=3,
            subperiod=None,
            timer_expiration=None,
            )

    return render(request, 'banking/participate.html', {
        'experiment_state': experiment_state,
        'bankNum': 1,
        'regionNum': 1,
        'earnings': '-4.30',
        'periodData': [
            {
                'deposits': '100.00',
                'depositDecision': '0.00',
                'depositFromOtherBank': '18.00',
                'lendingDecision': '0.00',
                'onDepositAtOtherBank': '0.00',
                'cash': '118.00',
                'loans': '0.00',
                'equity': '0.00',
                },
            {
                'withdrawals': '75.00',
                #'economy': economy,
                'recalledByOtherBank': '0.00',
                'totalOwed': '75.00',
                'paidFromCash': '75.00',
                'paidFromRecalledFunds': '0.00',
                'loansSold': '0.00',
                'paidFromLoans': '0.00',
                'paidFromBorrowing': '0.00',
                'totalPaid': '75.00',
                'cash': '43.00',
                'onDepositAtOtherBank': '0.00',
                'loans': '0.00',
                'deposits': '25.00',
                'depositFromOtherBank': '18.00',
                'borrowings': '0.00',
                'equity': '0.00',
                },
            {
                'bankPayoff': '0.00',
                'loanPayoff': '0.00',
                'withdrawals': '27.50',
                'withdrawalByOtherBank': '19.80',
                'totalOwed': '47.30',
                'paidFromCash': '43.00',
                'paidFromBorrowing': '4.30',
                'totalPaid': '47.30',
                'cash': '0.00',
                'onDepositAtOtherBank': '0.00',
                'loans': '0.00',
                'deposits': '0.00',
                'depositFromOtherBank': '0.00',
                'borrowings': '4.30',
                'equity': '-4.30',
                },
            ]
        })

def gettime(request):
    """
    For use with NTP.js from http://jehiah.cz/a/ntp-for-javascript

    From that page:

    NTP.sync() will make a configurable number of requests to the server. It
    passes the client timestamp in milliseconds UTC time.

    GET /sandbox/gettime?t=1130036017399
 
    Creates the following response with the offset and a copy of the timestamp
    which the offset is based. The following response shows an 11.1 second
    offset.
 
    11195:1130036017399
    """

    try:
        client_time = int(request.GET['t'])
    except:
        raise Http404

    # This should be equivalent to JavaScript's Date.getTime()
    # which is what's sent by the client.
    # It's the number of milliseconds since the Unix Epoch, UTC.
    server_time = int(time.time() * 1000)

    # The client ends up averaging delay-compensated offsets to its own time.
    # serverTime = clientTime + offset  --> offset = serverTime - clientTime
    offset = server_time - client_time

    return HttpResponse('{0}:{1}'.format(offset, client_time))

def server_clock(request):
    """
    Show a clock representing the current time on the server.
    """

    return render(request, 'banking/server-clock.html')
