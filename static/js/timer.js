$(document).ready(function() {
    setTimeout(updateTimer, 0);
});

function updateTimer() {

    // Get the current server time, NTP-corrected (see NTP.js)
    var curTime = new Date();
    curTime = new Date(NTP.fixTime(curTime.getTime()));

    // Calculate milliseconds remaining on the timer
    var ms_remaining = timer_expiration - curTime.getTime();
    if (ms_remaining < 0) {
        ms_remaining = 0;
    }

    // Update the displayed time based on rounded seconds remaining
    var sec_remaining = Math.round(ms_remaining / 1000.0);
    var min = Math.floor(sec_remaining / 60);
    var sec = sec_remaining % 60;
    if (sec < 10) {
        sec = '0' + sec;
    }
    $('.timer').text(min + ':' + sec);

    // If there is time left, schedule the next timer update so that it occurs
    // the next time there is a whole number of seconds remaining
    if (ms_remaining > 0) {
        setTimeout(updateTimer, ms_remaining % 1000);
        
        // Disable experimenter Advance button
        $('#advance_button').button('disable');

    } else {
        // Time is up
        
        // Disable decision form inputs
        $('#decision_form input').attr('disabled', 'disabled');
        $('#decision_form input[type=submit]').button('disable');

        // If this is not the last period of the last round, prepare for the
        // next period
        if (!(round_number == num_rounds && period_number == 3)) {

            // Enable experimenter Advance button
            $('#advance_button').button('enable');

            // Auto-advance if enabled
            if ($('#auto_advance').prop('checked')) {
                $('#advance_form').submit();
            }

            // Start polling the server for the next value of timer_expiration,
            // which will determine when the page should be reloaded
            setInterval(poll_timer_expiration, 1000);
        }
    }
}

function poll_timer_expiration() {
    $.get(
        timer_expiration_url,
        function(data) {
            new_timer_expiration = parseInt(data);
            if (new_timer_expiration != timer_expiration) {
                location.reload(true);
            }
        });
}
