$(document).ready(function() {

    $('#visibility-change-link').click(function() {
        $('#visibility-table').hide();
        $('#visibility-form').show();
    });

    $('#visibility-cancel-button').click(function() {
        $('#visibility-table').show();
        $('#visibility-form').hide();
    });

    $('#reset-experiment-form').submit(function() {
        return confirm("Are you sure you want to DELETE all experiment data and reset the experiment?");
    });

    $('form[action=remove-participant]').submit(function() {
        var participant_str = $(this).closest('tr').find(
            '.participant_str').text();
        return confirm("Are you sure you want to remove participant " +
                       participant_str + "?")
    });
});
