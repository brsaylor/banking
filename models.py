# Copyright 2014 University of Alaska Anchorage Experimental Economics
# Laboratory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
from decimal import Decimal

from django.db import models

from vcweb.core import signals, simplecache
from vcweb.core.models import ExperimentMetadata, Experiment,\
        ParticipantGroupRelationship

@simplecache
def get_experiment_metadata(refresh=False):
    return ExperimentMetadata.objects.get(namespace='banking')

def set_test_timer(seconds):
    state = ExperimentState.objects.get(experiment__isnull=True)
    now_ms = int(time.time() * 1000)
    state.timer_expiration = int((time.time() + seconds) * 1000)
    state.save()

class RoundParameterSet(models.Model):
    """
    The set of parameters for a specific round of a specific experiment
    """

    TIME_LIMIT_CHOICES = (
            (5, '5 sec'),
            (30, '30 sec'),
            (60, '1 min'),
            (120, '2 min'),
            (180, '3 min'),
            )

    experiment = models.ForeignKey(Experiment)
    round_number = models.PositiveIntegerField()

    practice = models.BooleanField(
            default=False,
            help_text="Make this a practice round " +
            "(equity will not be added to total earnings)")
    hedging = models.BooleanField(
            default=False,
            help_text="Incorporate hedging with interbank deposits")
    period_1_deposit_time_limit = models.PositiveIntegerField(
            choices=TIME_LIMIT_CHOICES, default=60,
            help_text="Time limit (seconds) for deposit decision in Period 1")
    period_1_lending_time_limit = models.PositiveIntegerField(
            choices=TIME_LIMIT_CHOICES, default=60,
            help_text="Time limit (seconds) for lending decision in Period 1")
    period_2_time_limit = models.PositiveIntegerField(
            choices=TIME_LIMIT_CHOICES, default=60,
            help_text="Time limit (seconds) for Period 2")
    period_3_time_limit = models.PositiveIntegerField(
            choices=TIME_LIMIT_CHOICES, default=60,
            help_text="Time limit (seconds) for Period 3")
    impatient_withdrawals = models.IntegerField(
            default=25,
            help_text="Total withdrawals by impatient depositors " +
            "in period 2 in the \"normal\" economy")
    complementarity = models.BooleanField(default=True,
            help_text="If True (checked), the \"Impatient withdrawals\" "
            "parameter defines the number of impatient depositors in the "
            "region with the \"normal\" economy. That region is selected "
            "randomly, and the other region has the \"recession\" economy with "
            "impatient depositors equal to 100 minus withdrawals in the first "
            "region. If set to False (unchecked), a random draw determines "
            "whether both regions have the \"normal\" economy, or both have "
            "the \"recession\" economy.  If both are \"normal\", they have a "
            "number of impatient depositors equal to the \"Impatient "
            "withdrawals\" parameter. If both are \"recession\", they have a "
            "number of impatient depositors equal to 100 minus the \"Impatient "
            "withdrawals\" parameter.")
    deposit_interest_rate = models.DecimalField(
            default=Decimal('0.25'),
            max_digits=3, decimal_places=2,
            help_text="Interest rate for patient depositors")
    loan_interest_rate = models.DecimalField(
            default=Decimal('0.5'),
            max_digits=3, decimal_places=2,
            help_text="Interest rate for mature loans")
    bank_interest_rate = models.DecimalField(
            default=Decimal('0.25'),
            max_digits=3, decimal_places=2,
            help_text="Interest rate for patient interbank deposits")
    loan_price = models.DecimalField(
            default=Decimal('0.4'),
            max_digits=3, decimal_places=2,
            help_text="Price of loans sold in period 2")

    class Meta:
        unique_together = ('experiment', 'round_number')
        ordering = unique_together

class ExperimentState(models.Model):
    """
    Represents the current state of a specific experiment
    """

    # If null, this is the ExperimentState used by participate-test
    experiment = models.OneToOneField(Experiment, null=True, blank=True)

    round_number = models.PositiveIntegerField()
    period_number = models.PositiveIntegerField()
    subperiod = models.CharField(
            null=True, blank=True,
            max_length=20,
            choices=(('deposit', 'deposit'), ('lending', 'lending')))

    # The time at which the period timer will expire, measured in milliseconds
    # since the Unix Epoch (corresponding to JavaScript's Date.getTime())
    timer_expiration = models.BigIntegerField(null=True, blank=True)

    @property
    def ms_remaining(self):
        if self.timer_expiration is None:
            return 0
        else:
            return max(0, self.timer_expiration - int(time.time() * 1000))

    @property
    def period_display_number(self):
        if self.period_number == 1 and self.subperiod is not None:
            if self.subperiod == 'deposit':
                return '1A: deposit decision'
            else:
                return '1B: lending decision'
        else:
            return str(self.period_number)

    def set_timer_sec(self, seconds):
        self.timer_expiration = int((time.time() + seconds) * 1000)

class PeriodData(models.Model):
    """
    Represents information about a specific participant in a specific experiment
    during a specific round and period. Each row in the main output file
    corresponds to one model instance
    """

    participant_group_relationship = models.ForeignKey(
            ParticipantGroupRelationship)
    round_number = models.PositiveIntegerField()
    period_number = models.PositiveIntegerField()

    cash = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    onDepositAtOtherBank = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    loans = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    deposits = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    depositFromOtherBank = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    borrowings = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    depositDecision = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    lendingDecision = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    withdrawals = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    recalledByOtherBank = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    totalOwed = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    paidFromCash = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    paidFromRecalledFunds = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    loansSold = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    paidFromLoans = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    paidFromBorrowing = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    withdrawalByOtherBank = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    bankPayoff = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    loanPayoff = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    earnings = models.DecimalField(
            null=True, max_digits=9, decimal_places=2)
    economy = models.CharField(
            null=True, max_length=20)

    @property
    def equity(self):
        return ((self.cash + self.onDepositAtOtherBank + self.loans)
                - (self.deposits + self.depositFromOtherBank + self.borrowings))

    @property
    def group_number(self):
        return self.participant_group_relationship.group_number

    @property
    def bank_number(self):
        return self.participant_group_relationship.participant_number

    @property
    def region_number(self):
        return (self.bank_number - 1) % 2 + 1

    @property
    def depositee_number(self):
        """ The bank_number of this bank's depositee bank """
        return (self.bank_number %
                self.participant_group_relationship.group.size
                + 1)

    @property
    def depositor_number(self):
        """ The bank_number of this bank's depositor bank """
        return ((self.bank_number - 2)
                % self.participant_group_relationship.group.size
                + 1
                )

    class Meta:
        unique_together = (
                'participant_group_relationship',
                'round_number',
                'period_number')
        ordering = ('round_number', 'period_number')
